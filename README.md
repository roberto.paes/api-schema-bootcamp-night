# api-auto-schema
Esta é uma pequena api que desenvolvi para o bootcamp do instituto infnet. Usando um documento json para armazenamento de dados.

Por que usa essa API para teste?

Esta API pode armazenar dados usando um arquivo json. Mas você pode criar seu esquema
e a API criará automaticamente suas rotas. Fácil de usar!


| Comando | Argumentos | Ação |
| - |:-: | -: |
| about | about \<database\> | mostra uma informação sobre um comando e mostra todos os comandos, se digitados sem argumentos |
| create | create \<database\>  | criar uma coleção |
| drop | drop \<database\>  | excluir uma coleção e sua configuração |
| tag | tag \<database\> \<generateall ou none \> \<tags separadas por vírgula \> | Crie um campo de tags no documento json, para pesquisa avançada |
| relation | relation \<pai\> \<filho\> \<chave\> | Crie uma relação entre as coleções pai e filho |
| language | language \<nome do arquivo\> | Define o idioma |
| remove | remove \<coleção\> \<tag ou relação\> | Remove uma relação ou marca de uma coleção específica. |
| clear | clear \<coleção\> | Limpar uma coleção |

Quando você cria uma coleção, a api ativa a sintaxe para estas rotas:


| Rota | verbo | Ação |
| - |:-: | -: |
| /v1/{coleção} | get | obter todos os dados de coleta |
| v1/{coleção} /: id | get | obter dados de coleta específicos |
| v1/{coleção} /: id | delete | deletar dados de coleta específicos |
| v1/{coleção} | put | atualizar uma coleção de dados |
| v1/{coleção} | post | criar uma coleção de dados |
| v1/{coleção} / extra | get | obtenção de dados relacionais avançados. Para que esta rota funcione, você precisa fornecer os nomes das tabelas relacionais no parâmetro query. Você pode passar um id específico para obter um dado específico de sua coleção. Todos esses parâmetros precisam ser inseridos na url
| v1 /{coleção} / pesquisa | get | relacionais avançados obtêm dados com base em tags de pesquisa. Para que essa rota funcione, você pode fornecer os nomes das tabelas relacionais no parâmetro de relações. Você precisa passar as tags que deseja encontrar no parâmetro de consulta. Todos esses parâmetros precisam ser inseridos na url| 
